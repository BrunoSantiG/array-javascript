# Array

##### Split

A função `split()` divide uma string retornando um array de strings.

Exemplo:

```javascript
const estados = "MG,BA,CE,SP,RJ";
const arranjo_estados = estados.split(",");
console.log(arranjo_estados);
//saida: ["MG","BA","CE","SP","RJ"]
```

##### Join

A função `join()` junta todos os elementos de uma array em uma string e retorna esta string.

Exemplo:

```javascript
const arranjo_estados = ["MG", "BA", "CE", "SP", "RJ"];
const estados = arranjo_estados.join(",");
console.log(estados);
//saida: "MG,BA,CE,SP,RJ"
```

##### Concat

A função `concat()` retorna um novo array contendo todos os arrays.

Exemplo:

```javascript
const estados1 = ["MG", "BA", "CE"];
const estados2 = ["SP", "RJ"];
const estados = estados1.concat(estados2);
console.log(estados);
//saida: ["MG","BA","CE","SP","RJ"]
```

##### Map, filter e reduce

Esse conjunto de funções facilita as operações com arranjos.

###### Map

`map()` invoca a função passada por argumento para cada elemento do Array e devolve um novo Array de mesmo tamanho como resultado.

```javascript
const arranjo = [20, 50, 60];
const novo_arranjo = arranjo.map(function (valor, index) {
	return valor * 2;
});

//const novo_arranjo = arranjo.map((valor, index) => {
//	return valor * 2;
//});

console.log(novo_arranjo);
//saida: [40,100,120]
```

###### Filter

`filter()` cria um novo array com todos os elementos que passaram no teste implementado pela função fornecida.

```javascript
const arranjo = [20, 50, 60];
const novo_arranjo = arranjo.filter(function (valor, index) {
	return valor > 40;
});

console.log(novo_arranjo);
//saida: [50,60]
```

###### Reduce

`reduce()` executa uma função para cada elemento do array, retornando um único valor.

```javascript
const arranjo = [20, 50, 60];
const resultado = arranjo.reduce(function (acumulador, valor) {
	return (acumulador = acumulador + valor);
}, 0);

console.log(resultado);
//saida: 130
```
