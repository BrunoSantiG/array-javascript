let response;
document.addEventListener("DOMContentLoaded", async function () {
	response = await axios.get("https://jsonplaceholder.typicode.com/users");
	console.log(response);
});

const returnList = (usuarios) => {
	return usuarios
		.map((usuario) => {
			return `<li>${usuario.name} - ${usuario.id}</li>`;
		})
		.join("");
};

const listAll = () => {
	const lista = document.getElementById("all");
	// <ul>
	//
	// </ul>
	const usuarios = response.data;

	lista.innerHTML = returnList(usuarios);
};

const listById = () => {
	const lista = document.getElementById("evenId");

	const usuarios = response.data;

	const usuarios_filtrados = usuarios.filter((usuario) => {
		return usuario.id % 2 === 0;
	});

	lista.innerHTML = returnList(usuarios_filtrados);
};

const sumId = () => {
	const h5 = document.getElementById("sum");

	const usuarios = response.data;

	h5.innerHTML =
		"Soma: " +
		usuarios.reduce((acumulador, usuario) => {
			return (acumulador += usuario.id);
		}, 0);
};

const addUsers = () => {
	let nomes = document.getElementById("nomes").value;

	document.getElementById("nomes").value = "";

	if (nomes) {
		nomes = nomes.split(",");

		const new_names = nomes.map((nome, index) => {
			return {
				id: response.data.length + index + 1,
				name: nome,
			};
		});

		response.data = response.data.concat(new_names);

		listAll();
		listById();
		sumId();
	}
};
